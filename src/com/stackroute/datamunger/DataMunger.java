package com.stackroute.datamunger;

import java.util.Scanner;

public class DataMunger {

	public static void main(String[] args) {

	        /* read a sentence from the user */
	        
	        System.out.println("Enter the input:");
	        Scanner sc = new Scanner(System.in);
	        String sentence = sc.nextLine();
	        
	        /*
	         * call getWordCount() method which should return no. of words present
	         * in the string and display the same
	         */
	    
	        System.out.println("Number of Words: "+getWordCount(sentence));
	        sc.close();
		    
	}
	
	

	/*
	 * This method is used to calculate the no. of words present in the given
	 * string. Please note that in a sentence, words are split by space. Please change the method return 
	 * type to int
	 */
    public static int getWordCount(String sentence)
    {
        
    	if (sentence != "")
    	{
    			String []senArray = sentence.split(" ");
    			int numberOfWord = senArray.length;
    			return numberOfWord;  
    	}
    	else
    	{
    		return 0;
    	}
    	
    };    

}
	

